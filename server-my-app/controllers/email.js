const config    = require('../config.js')
const nodemailer = require('nodemailer');

// Import pwd form form mailer where we can hide our mailbox password
// selecting mail service and authorazing with our credentials

// let transporter = nodemailer.createTransport({
//     host: 'smtp.gmail.com',
//     port: 465,
//     secure: true,
//     auth: {
//         type: 'OAuth2'
//     }
// });

// transporter.set('oauth2_provision_cb', (user, renew, callback) => {
//     let accessToken = userTokens[user];
//     if(!accessToken){
//         return callback(new Error('Unknown user'));
//     }else{
//         return callback(null, accessToken);
//     }
// });

const transport = nodemailer.createTransport({
  //you need to enable the less secure option on your gmail account
//https://myaccount.google.com/lesssecureapps?pli=1
	service: 'Gmail',
  port: 465,
	auth: {
		user: config.user,
		pass: config.pass,
	}});

const sendemail = async (req,res) => {
	  const { firstName, lastName , email , content } = req.body
	  console.log('contact ====>', firstName,lastName , email , content )
	  const default_subject = 'This is a default subject'
	  const mailOptions = {
		    to: 'evelinerojas@gmail.com',
		    subject: "New message from " + firstName,
		    html: content,
	   }
      try{
           const response = await transport.sendMail(mailOptions)
           console.log('=========================================> Email sent !!')
           return res.json({on:true,message:'email sent'})
      }
      catch( err ){
           return res.json({ok:false,message:err})
      }
}

module.exports = { sendemail }