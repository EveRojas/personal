const express = require('express');
    app = express();
    exphbs = require ('express-handlebars');
    mongoose = require('mongoose');
    path = require('path');
    nodemailer = require("nodemailer");
    email = require('./routes/email');
    config = require('./config');
    bodyParser = require('body-parser');
    cors = require ('cors');
    port = process.env.PORT || 9000;

//======view enfine setup=====//
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');

// ===== static folder ==== //
app.use ('/public', express.static(path.join(__dirname, 'public')))
// =================== initial settings ===================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// app.get('/', (req, res) => {
// 	res.render('contact');
// });

// app.post('/send_email', (req, res) => {
// console.log('req.body');
// });


// app.listen(9000, () => console.log(`server started on port 9000`))

//connnect to mongo

//connecting to mongo and checking if DB is running
async function connecting(){
try {
    await mongoose.connect('mongodb://localhost:9000/newdatabase', { useUnifiedTopology: true , useNewUrlParser: true })
    console.log('Connected to the DB')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up !!!');
}
}
connecting()
// temp stuff to suppress internal warning of mongoose which would be updated by them soon
mongoose.set('useCreateIndex', true);
// end of connecting to mongo and checking if DB is running

// routes
app.use('/email', email);
app.use('/email/sendemail', email);
// Set the server to listen on port 9000

app.listen(9000, () => console.log(`listening on port 9000`))
