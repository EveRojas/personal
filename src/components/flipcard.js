import { render, useHistory } from 'react-router-dom'
import React, { useState } from 'react'
import { useSpring, animated as a } from 'react-spring'
import { useDrag } from 'react-use-gesture'
import './styles.css'

function Flipcard() {
  const [flipped, set] = useState(false)
  const { transform, opacity } = useSpring({
    opacity: flipped ? 1 : 0,
    transform: `perspective(200px) rotateX(${flipped ? 180 : 0}deg)`,
    config: { mass: 5, tension: 500, friction: 80 }
  })


  return (
  <div>
    <div  
         onClick={() => set(state => !state)}>
      <a.div className="c back" style={{ opacity: opacity.interpolate(o => 1 - o), transform }} />
      <a.div className="c front" style={{ opacity, transform: transform.interpolate(t => `${t} rotateX(180deg)`) }} />
      <div className= "c"></div>
      <div className= "flip">🔘</div> 
    </div>
  </div>
  )
}

export default Flipcard;
