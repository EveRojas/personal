import { render } from 'react-dom'
import React, { useState, useCallback } from 'react'
import { useTransition, animated } from 'react-spring'
import './styles.css'

const pages = [
  ({ style }) => <animated.div style={{ ...style, background: 'lightpink' }}> Full-Stack Javascript Developer - Barcelona Code School > </animated.div>,
  ({ style }) => <animated.div style={{ ...style, background: 'lightblue' }}> Phd in Sociology - UFPE & University of Leeds > </animated.div>,
  ({ style }) => <animated.div style={{ ...style, background: 'lightgreen' }}> Social Tech - ContaMap Project Founder > </animated.div>,
]

function Transitioncard() {
  const [index, set] = useState(0)
  const onClick = useCallback(() => set(state => (state + 1) % 3), [])
  const transitions = useTransition(index, p => p, {
    from: { opacity: 0, transform: 'translate3d(100%,0,0)' },
    enter: { opacity: 1, transform: 'translate3d(0%,0,0)' },
    leave: { opacity: 0, transform: 'translate3d(-43%,0,0)' },
  })
  return (
    <div className="simple-trans-main" onClick={onClick}>
      {transitions.map(({ item, props, key }) => {
        const Page = pages[item]
        return <Page key={key} style={props} />
      })}
    </div>
  )
}

export default Transitioncard;
