import React, { Component } from "react";
import axios from 'axios'; 

class ContactForm extends Component {
  
constructor(props) {
    super(props);    

    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      content: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this);
  } 


    handleSubmit = async (event) => {
    event.preventDefault();

   
    let { firstName, lastName, email, content } = this.state
    console.log('state ====>', this.state)
    let url = `https://localhost:9000/email/sendemail`;
      
      try{
           const res = await axios.post(url,{firstName,lastName,email,content}); 
           console.log('res mail==>',res);
           alert('email sent!')
       
    } catch (error) {
      alert({error:'something went wrong'})      
    }
} 

 handleInputChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
}

  render() {
    const {firstName, lastName, email, content } = this.state;    
    return (
      <form onSubmit={this.handleSubmit} className="grid-form">
        <input
          className="contact"
          value={firstName}
          onChange={this.handleInputChange}
          placeholder="First name"
          type="text"
          name="firstName"
          required
        />
        <input
          className="contact"
          value={lastName}
          onChange={this.handleInputChange}
          placeholder="Last name"
          type="text"
          name="lastName"
          required
        />
        <input
          className="contact"
          value={email}
          onChange={this.handleInputChange}
          placeholder="Email address"
          type="email"
          name="email"
          required
        />
        <textarea
          className="contactcontent"
          value={content}
          onChange={this.handleInputChange}           
          placeholder="Content"
          type="content"
          name="content"
          required
        />       
         <button className="contactbutton" type="submit">Submit</button>
      </form>
    );
  }
}export default ContactForm;