import React, { useState, useRef  } from 'react'
import ReactDOM from 'react-dom';
import { useSpring, animated } from 'react-spring';
import { ThemeProvider } from 'styled-components';
import { useOnClickOutside } from '../hooks';
import { GlobalStyles } from '../global';
import { theme } from '../theme';
import { Burger, Menu } from '../menu';
import Card from '../components/card';
import Board from '../components/board';
import Flipcard from '../components/flipcard';
import Transitioncard from '../components/transitioncard';
import ContactCall from './contactcall';
import ContactForm from './contactform';
import FocusLock from 'react-focus-lock';
import MessengerCustomerChat from 'react-messenger-customer-chat';

function Contact() {
  const [open, setOpen] = useState(false);
  const node = useRef();
  const menuId = "main-menu";

  useOnClickOutside(node, () => setOpen(false));

   return (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyles />
        <div ref={node}>
          <FocusLock disabled={!open}>
            <Burger open={open} setOpen={setOpen} aria-controls={menuId} />
            <Menu open={open} setOpen={setOpen} id={menuId} />
          </FocusLock>
        </div>

        <div>
        <ContactCall/>
        </div>
        <div>
        <Flipcard/>
        </div>  
         <div>
        <ContactForm/>
        </div>
      <div>
      <small className= "clue">Know <a href="https://www.gitlab.com/EveRojas">more</a></small>
      </div>
  <div>
    <MessengerCustomerChat
      pageId="<265745757449510>"
      appId="<528895694684342>/"
    />
  </div>
      </>
    </ThemeProvider>
  );
}

export default Contact;



