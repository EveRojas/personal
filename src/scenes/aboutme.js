import React, { useState, useRef } from 'react';
import { ThemeProvider } from 'styled-components';
import { useOnClickOutside } from '../hooks';
import { GlobalStyles } from '../global';
import { theme } from '../theme';
import { Burger, Menu } from '../menu';
import Card from '../components/card';
import Board from '../components/board';
import Flipcard from '../components/flipcard';
import Transitioncard from '../components/transitioncard';
// import Fadecard from './components/fadecard';
import Textcard from '../components/textcard';
import Highlight from '../components/highlight';
import Guess from '../components/guess';
import Guess2 from '../components/guess2';
import AboutMeCall from './aboutmecall';
import {Badge} from './badge.png';
import FocusLock from 'react-focus-lock';

function AboutMe() {
  const [open, setOpen] = useState(false);
  const node = useRef();
  const menuId = "main-menu";

  useOnClickOutside(node, () => setOpen(false));

   return (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyles />
        <div ref={node}>
          <FocusLock disabled={!open}>
            <Burger open={open} setOpen={setOpen} aria-controls={menuId} />
            <Menu open={open} setOpen={setOpen} id={menuId} />
          </FocusLock>
        </div>
        <div>
        <AboutMeCall/>
        </div>
        <div>
        <Flipcard/>
        </div>
        <div>
        <Transitioncard/>  
        </div>
      <div>
      <small className= "clueabout">ACCESS<a href='https://www.linkedin.com/in/eveline-rojas-264857a7'> CV</a></small>
      </div>
      </>
    </ThemeProvider>
  );
}

export default AboutMe;



