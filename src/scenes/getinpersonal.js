import React from "react";
//import { library } from '@fortawesome/fontawesome-svg-core';
import { faLinkedin, faTwitter,faInstagram } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


export default function GetInPersonal(){
  return(
    <div className='social-container'>
    <a href='https://www.linkedin.com/in/eveline-rojas-264857a7'
       className="social-icons">
       <FontAwesomeIcon icon={faLinkedin} size="1x"/>
    </a>
    <a href='https://twitter.com/listash/'
       className="social-icons">
       <FontAwesomeIcon icon={faTwitter} size="1x"/>
    </a>
        <a href='https://www.instagram.com/evelinerojas'
       className="social-icons">
       <FontAwesomeIcon icon={faInstagram} size="1x"/>
    </a>
    </div>
  );
}
