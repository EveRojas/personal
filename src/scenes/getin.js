import React, { useState, useRef } from 'react';
import { ThemeProvider } from 'styled-components';
import { useOnClickOutside } from '../hooks';
import { GlobalStyles } from '../global';
import { theme } from '../theme';
import { Burger, Menu } from '../menu';
import Card from '../components/card';
import Board from '../components/board';
import Flipcard from '../components/flipcard';
import Transitioncard from '../components/transitioncard';
import Textcard from '../components/textcard';
import Highlight from '../components/highlight';
import FocusLock from 'react-focus-lock';
import GetInCall from './getincall';
import GetInMedia from './getinmedia';
import GetInPersonal from './getinpersonal';
import { faLinkedin, faTwitter,faInstagram } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';



function GetIn() {
  const [open, setOpen] = useState(false);
  const node = useRef();
  const menuId = "main-menu";

  useOnClickOutside(node, () => setOpen(false));

   return (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyles />
        <div ref={node}>
          <FocusLock disabled={!open}>
            <Burger open={open} setOpen={setOpen} aria-controls={menuId} />
            <Menu open={open} setOpen={setOpen} id={menuId} />
          </FocusLock>
        </div>
        <div>
        <GetInCall/>
        </div>
        <div>
                       <Flipcard/>
        </div> 
        <div className="follow">
        <GetInMedia/>
        To Follow 
        <GetInPersonal/> 
        </div>
         
      <div>
      <small className= "clue">Know <a href="https://www.gitlab.com/EveRojas"> More</a></small>
      </div>
      </>
    </ThemeProvider>
  );
}

export default GetIn;



