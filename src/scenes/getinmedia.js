import React from "react";
//import { library } from '@fortawesome/fontawesome-svg-core';
import { faFacebook, faTwitter,faInstagram } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


export default function GetInMedia(){
  return(
    <div className='social-container'>
    <a href='https://www.facebook.com/contamap.org/'
       className="social-icons">
       <FontAwesomeIcon icon={faFacebook} size="1x"/>
    </a>
    <a href='https://twitter.com/MapConta/'
       className="social-icons">
       <FontAwesomeIcon icon={faTwitter} size="1x"/>
    </a>
        <a href='https://www.instagram.com/conta_map'
       className="social-icons">
       <FontAwesomeIcon icon={faInstagram} size="1x"/>
    </a>
    </div>
  );
}
