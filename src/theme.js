export const theme = {
  primaryDark: '#7e8381',
  primaryLight: '#f4e63b',
  primaryHover: '#171716',
  mobile: '576px',
}

// #e3fc00