import React, { useState, useRef } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { ThemeProvider } from 'styled-components';
import { useOnClickOutside } from './hooks';
import { GlobalStyles } from './global';
import { theme } from './theme';
import { Burger, Menu } from './menu';
import Card from './components/card';
import Board from './components/board';
import Flipcard from './components/flipcard';
import Transitioncard from './components/transitioncard';
// import Fadecard from './components/fadecard';
import Textcard from './components/textcard';
import Highlight from './components/highlight';
import Guess from './components/guess';
import Guess2 from './components/guess2';
import FocusLock from 'react-focus-lock';
import GetIn from './scenes/getin';
import Home from './scenes/home';
import AboutMe from './scenes/aboutme';
import Contact from './scenes/contact';

function App() {
  const [open, setOpen] = useState(false);
  const node = useRef();
  const menuId = "main-menu";

  useOnClickOutside(node, () => setOpen(false));

  return (
    <Router>
  
    <ThemeProvider theme={theme}/>
     <Route exact path="/"      component={Home} />
          <Route path="/getin" component={GetIn} />
          <Route path="/aboutme" component={AboutMe} />
          <Route path="/contact" component={Contact} />
    </Router>
  );
}

export default App;





 // 
 //          <h1>Eve's</h1>
 //          <small>Know <a href="https://www.gitlab.com/EveRojas">More</a></small>
 //        </div>