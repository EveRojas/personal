import React from 'react';
import { bool } from 'prop-types';
import { StyledMenu } from './Menu.styled';
import { NavLink } from "react-router-dom";

const Menu = ({ open, ...props }) => {
  
  const isHidden = open ? true : false;
  const tabIndex = isHidden ? 0 : -1;

  return (
    <StyledMenu open={open} aria-hidden={!isHidden} {...props}>
     

      <NavLink
        exact
       tabIndex={tabIndex}
       to={"/"}
      >
      <span aria-hidden="true"></span>
        Home 
      </NavLink>
   
      <NavLink
        exact
       tabIndex={tabIndex}
       to={"/aboutme"}
      >
      <span aria-hidden="true"></span>
        About me
      </NavLink>   
   
      <NavLink
        exact
       tabIndex={tabIndex}
       to={"/getin"}
      >
      <span aria-hidden="true"></span>
        Get in 
      </NavLink>
      
      <NavLink
        exact
       tabIndex={tabIndex}
       to={"/contact"}
      >
      <span aria-hidden="true"></span>
        Contact 
      </NavLink>
      
    </StyledMenu>
  )
}

Menu.propTypes = {
  open: bool.isRequired,
}

export default Menu;